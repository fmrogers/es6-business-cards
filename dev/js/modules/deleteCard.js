/**
 * @package: Delete Card
 * @name: deleteCard
 * @param: cardContainer (element selector)
 * @return: none
 * @description: remove child elements from the containing element.
 * @requires: none
 */

const deleteCard = (cardContainer) => {

    cardContainer.addEventListener('click', (event) => {

        const target = event.target;
        const targetClass = target.className;
        const row = target.closest('.row');
        const card = target.closest('.card').parentNode;

        if (targetClass == 'delete') {

            row.removeChild(card);

        }

    });


};

export default deleteCard;

