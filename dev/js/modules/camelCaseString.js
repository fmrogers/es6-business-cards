/**
 * @package: CamelCaseString (CCS)
 * @name: camelCaseString
 * @param: string (string)
 * @param: seperator (string)
 * returns: string
 * @description: Accepts 2 parameters both string values.
 * The first being the string you want to convert to camelcase,
 * the second being the character that separates each word in
 * the string value ie. "-" or "_" and returns a copy of the
 * string in in camelcase format. Example "foo-bar" becomes "fooBar"
 */

const camelCaseString = (string, seperator) => {

    const newString = [];

    string = string.split(seperator);

    newString.push(string[0]);

    for ( let i = 0; i < string.length; i++ ) {

        if ( i !== 0) {

            const capitalizedString = string[i].substring(0,1).toUpperCase() + string[i].substring(1, string[i.length]);

            newString.push(capitalizedString);

        }

    }

    return newString.join("");

};

export default camelCaseString;