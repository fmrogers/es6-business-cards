/**
 * @package: Render Business Card
 * @name: array (array)
 * @param: index (int)
 * @param: destination (node)
 * @return: string
 * @description: Accepts three parameters, an array
 * of objects and integers representing the array index
 * as well as the node where the template is to be placed.
 * @requires: none
 */

const renderCard = (array, index, destination) => {

    const card = array;

    let template = `<div id="card-${index}" class="col-4">
                        <div class="card card-contact mb-4">
                            <div class="card-body">
                                <button type="button" class="close" aria-label="Close">
                                    <span class="delete" aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="card-title">${card[index].firstName} ${card[index].lastName}</h4>
                                <h5 class="card-sub-title">${card[index].jobTitle} <small class="d-block">at ${card[index].employer}</small></h5>
                                <a class="d-block" href="tel:+47${card[index].phoneNum}">+47 ${card[index].phoneNum}</a>
                                <a class="d-block" href="mailto:${card[index].emailAddress}">${card[index].emailAddress}</a>
                            </div>
                        </div>
                    </div>`;

    destination.innerHTML += template;

};

export default renderCard;