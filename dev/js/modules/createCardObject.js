/**
 * @package: Create Card Object
 * @name: createCardObject
 * @param: inputFields (NodeList/array)
 * @return: none
 * @description: Accepts a NodeList of matched elements
 * and loops through each elements collecting values and
 * element id's before storing the data in a object and
 * pushing the object to a existing array(see param)
 * @requires: camelCaseStrings
 */

// Required imports
import camelCaseString from "./camelCaseString";

const createCardObject = (formInputs) => {

    const object = {};

    for (let i = 0; i < formInputs.length; i++) {

        const key = camelCaseString(formInputs[i].id, "-");

        object[key] = formInputs[i].value;

    }

    return object;

};

export default createCardObject;