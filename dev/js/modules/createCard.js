/**
 * @package: Create Card
 * @name: createCard
 * @param: formSelector
 * @param: cardContainer
 * @return: none
 * @description: Bind a submit event to the form element
 * and creates a object of data collected from input
 * fields and pushes to a array of objects.
 * @requires: createCardObject, renderCard
 */

// Required Imports
import createCardObject from './createCardObject';
import renderCard from './renderCard';

const createCard = (formSelector, cardContainer) => {

    const cardsArray = [];
    const inputs = formSelector.querySelectorAll('input');

    let cardCounter = 0;

    // Bind click event to for buttons
    formSelector.addEventListener('submit', (event) => {

        event.preventDefault();

        cardsArray.push(createCardObject(inputs));

        renderCard(cardsArray, cardCounter, cardContainer);

        cardCounter++;

    });

}

export default createCard;