/**
 * @package: ES6 Business Cards
 * @description: Dynamically generated business cards using ES6 standard
 * @author: Frederick M. Rogers
 * @description:
 * @requires: createCard
 */

// Required Imports
import createCard from './modules/createCard';
import deleteCard from './modules/deleteCard';

// Element selectors
const form = document.getElementById('business-card-form');
const cardContainer = document.getElementById('card-list').querySelector('.row');

// Waits for the entirety of the HTML structure
// to load minus stylesheet and images ect.
document.addEventListener('DOMContentLoaded', () => {

    // Initialise functions
    createCard(form, cardContainer);
    deleteCard(cardContainer);

});




