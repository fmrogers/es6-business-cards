// Requirements
var gulp            = require( 'gulp' );
var rename          = require( 'gulp-rename' );
var sass            = require( 'gulp-sass' );
var uglify          = require( 'gulp-uglify' );
var autoprefixer    = require( 'gulp-autoprefixer' );
var sourcemaps      = require( 'gulp-sourcemaps' );
var browserify      = require( 'browserify' );
var babelify        = require( 'babelify' );
var source          = require( 'vinyl-source-stream' );
var buffer          = require( 'vinyl-buffer' );

// BrowserSync
var browserSync     = require( 'browser-sync' ).create();
var reload          = browserSync.reload;

// HTML
var htmlWATCH       = './**/*.html';

// CSS Style
var styleSRC        = 'dev/scss/business-cards.scss';
var styleDIST       = './css/';
var styleWATCH      = 'dev/scss/**/*.scss';

// JavaScript
var jsSRC           = 'business-cards.js';
var jsFOLDER        = 'dev/js/';
var jsDIST          = './js/';
var jsWATCH         = 'dev/js/**/*.js';
var jsFILES         = [jsSRC];

gulp.task('browser-sync', function() {

    browserSync.init({
        open: false,
        injectChanges: true,
        server: {
            baseDir: "./"
        }
    });

});

gulp.task('style', function() {

    gulp.src( styleSRC )
        .pipe( sourcemaps.init() )
        .pipe( sass({
            errorLogToConsole: true,
            outputStyle: 'compressed'
        }) )
        .on( 'error', console.error.bind( console ) )
        .pipe( autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }) )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( sourcemaps.write( './' ) )
        .pipe( gulp.dest( styleDIST ) )
        .pipe( browserSync.stream() );

});

gulp.task ('js', function() {

    jsFILES.map(function( entry ) {
        return browserify({
            entries: [jsFOLDER + entry]
        })
            .transform( babelify, {presets: ['env']} )
            .bundle()
            .pipe( source( entry ) )
            .pipe( rename( {extname: '.min.js'}) )
            .pipe( buffer() )
            .pipe( sourcemaps.init({ loadMaps: true }) )
            .pipe( uglify() )
            .pipe( sourcemaps.write( './' ) )
            .pipe( gulp.dest( jsDIST ) )
            .pipe( browserSync.stream() );
    });

});

gulp.task('default', ['style', 'js']);

gulp.task('watch', ['default', 'browser-sync'], function() {

    gulp.watch( htmlWATCH, reload );
    gulp.watch( styleWATCH, ['style', reload] );
    gulp.watch( jsWATCH, ['js', reload] );

});